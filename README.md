# timelinejs-local

Allow to use local csv files with [timelinejs](https://github.com/NUKnightLab/TimelineJS3) library.

Demo hosted by framasoft: http://eduinfo.frama.io/timelinejs-local

## Quick start

Just copy the public folder and modify `timeline-data.csv` with your favorite spreadsheet editor.

## Dev

- First load your submodules

- Hack on the `src` directory

- commit changes:

      git commit

- Copy changes to public directory for example with rsync:

      rsync -rL src/ public
